package net.cubekrowd.hopper;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class LocalVariableClassRemapper extends ClassVisitor {
    public Remapper2 remapper;

    public LocalVariableClassRemapper(ClassVisitor delegate, Remapper2 remapper) {
        super(Opcodes.ASM9, delegate);
        this.remapper = remapper;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        var visitor = super.visitMethod(access, name, descriptor, signature, exceptions);
        var res = new MethodRemapper(visitor);
        return res;
    }

    public class MethodRemapper extends MethodVisitor {
        public int subtract = 0;

        public MethodRemapper(MethodVisitor delegate) {
            super(Opcodes.ASM9, delegate);
        }

        @Override
        public void visitLocalVariable(String name, String descriptor, String signature, Label start, Label end, int index) {
            // @NOTE(traks) the built-in remapper already remaps the descriptor.
            // It just doesn't allow you to remap the name.
            if (name.equals("this")) {
                subtract = 1;
                super.visitLocalVariable(name, descriptor, signature, start, end, index);
            } else {
                super.visitLocalVariable(remapper.mapLocalVariableName(name, descriptor, index - subtract), descriptor, signature, start, end, index);
            }
        }
    }
}
