package net.cubekrowd.hopper;

import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public interface JAREntryConsumer {
    void accept(ZipInputStream zis, ZipEntry entry) throws IOException;
}
