package net.cubekrowd.hopper;

import net.cubekrowd.hopper.mapping.ObfuscationMapping;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

// Loads super classes and interfaces into obfuscation mappings, so the
// deobfuscator can also deobfuscate member names that are not present in the
// mapping for the class itself, but are for super types.
public final class SuperProvider extends ClassVisitor {
    private final ObfuscationMapping mapping;

    public SuperProvider(ObfuscationMapping mapping) {
        super(Opcodes.ASM9);
        this.mapping = mapping;
    }

    @Override
    public void visit(int version, int access, String name, String signature,
            String superName, String[] interfaces) {
        var type = Type.getObjectType(name);

        if (superName != null) {
            mapping.addSuper(type, Type.getObjectType(superName));
        }
        if (interfaces != null) {
            for (var interfaceName : interfaces) {
                mapping.addSuper(type, Type.getObjectType(interfaceName));
            }
        }
    }
}
