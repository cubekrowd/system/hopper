package net.cubekrowd.hopper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.Duration;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.json.Json;
import javax.json.JsonObject;
import net.cubekrowd.hopper.mapping.ObfuscationMapping;
import net.cubekrowd.hopper.mapping.ProGuardMappingMaker;
import net.cubekrowd.hopper.mapping.ProGuardMappingReader;
import org.jd.core.v1.ClassFileToJavaSourceDecompiler;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.ClassRemapper;
import picocli.CommandLine;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@CommandLine.Command(name = "hopper")
public final class Hopper implements Runnable {
    @Parameters(paramLabel = "out", description = "deobfuscated output JAR")
    private File output;

    private static final class Input {
        @Option(names = {"-c", "--client"}, description = "use Minecraft client")
        private boolean useClient;

        @Option(names = {"-s", "--server"}, description = "use Minecraft server")
        private boolean useServer;
    }

    @ArgGroup(exclusive = true, multiplicity = "1")
    private Input input;

    @Option(names = {"-v", "--version"}, description = "version to use, 'release', 'snapshot' or version JSON file")
    private String inputVersion = "release";

    @Option(names = {"-d", "--decompile"}, description = "decompile the deobfuscated JAR")
    private boolean decompile;

    @Option(names = {"-j", "--jar"}, description = "JAR to deobfuscate")
    private String jarPath;

    @Option(names = {"-m", "--mapping"}, description = "mapping to apply")
    private String mappingPath;

    @Option(names = {"-k", "--keep-locals"}, description = "don't rename local variables")
    private boolean keepLocals;

    private JsonObject fetchJson(HttpClient httpClient, String url) {
        var request = HttpRequest.newBuilder(URI.create(url)).GET().build();
        HttpResponse<String> response = null;
        try {
            response = httpClient.send(request,
                    HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
        var reader = Json.createReader(new StringReader(response.body()));
        return reader.readObject();
    }

    private byte[] download(HttpClient httpClient, String url) {
        var request = HttpRequest.newBuilder(URI.create(url)).GET().build();
        try {
            var response = httpClient.send(request, HttpResponse.BodyHandlers.ofByteArray());
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void iterateJARFiles(byte[] jar, JAREntryConsumer consumer) {
        // @NOTE(traks) Use ZIP streams so we don't need special handling to
        // deal with the MANIFEST.MF
        try (var fis = new ByteArrayInputStream(jar)) {
            var zis = new ZipInputStream(fis);

            for (;;) {
                var entry = zis.getNextEntry();
                if (entry == null) {
                    break;
                }

                if (entry.getName().endsWith(".jar")) {
                    iterateJARFiles(zis.readAllBytes(), consumer);
                } else {
                    consumer.accept(zis, entry);
                }

                zis.closeEntry();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        // @TODO(traks) There's a small issue where the user could
        // provide a version string AND a jar even if their versions
        // conflict. I guess if people provide jars and mappings, we
        // should trust them they are compatible.

        byte[] jar = null;
        byte[] mappingData = null;

        String type = input.useClient ? "client" : "server";
        String actualVersion = null;

        if (jarPath == null || mappingPath == null) {
            var httpClient = HttpClient.newBuilder()
                    .connectTimeout(Duration.ofSeconds(5))
                    .followRedirects(HttpClient.Redirect.NEVER)
                    .version(HttpClient.Version.HTTP_2)
                    .build();

            JsonObject versionInfo;

            if (inputVersion.endsWith(".json")) {
                System.out.println("loading version JSON file");

                try (var is = new FileInputStream(inputVersion)) {
                    versionInfo = Json.createReader(new BufferedInputStream(is)).readObject();
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }

                actualVersion = versionInfo.getString("id");
            } else {
                System.out.println("downloading version list");

                var versionsSpec = fetchJson(httpClient,
                        "https://piston-meta.mojang.com/mc/game/version_manifest_v2.json");

                if (inputVersion.equals("release")) {
                    actualVersion = versionsSpec.getJsonObject("latest").getString("release");
                } else if (inputVersion.equals("snapshot")) {
                    actualVersion = versionsSpec.getJsonObject("latest").getString("snapshot");
                } else {
                    actualVersion = inputVersion;
                }

                var versions = versionsSpec.getJsonArray("versions");
                String url = null;
                for (int i = 0; i < versions.size(); i++) {
                    var spec = versions.getJsonObject(i);
                    var id = spec.getString("id");
                    if (id.equals(actualVersion)) {
                        url = spec.getString("url");
                        break;
                    }
                }

                if (url == null) {
                    System.out.println("version not found");
                    return;
                }

                System.out.println("downloading version specification");

                versionInfo = fetchJson(httpClient, url);
            }

            var downloads = versionInfo.getJsonObject("downloads");
            var jarUrl = downloads.getJsonObject(type).getString("url");
            var mappingDataUrl = downloads.getJsonObject(type + "_mappings").getString("url");

            if (jarPath == null) {
                System.out.println("downloading " + type + " version " + actualVersion);
                jar = download(httpClient, jarUrl);
            }
            if (mappingPath == null) {
                System.out.println("downloading mapping");
                mappingData = download(httpClient, mappingDataUrl);
            }
        }

        if (jarPath != null) {
            System.out.println("loading JAR");
            try {
                jar = Files.readAllBytes(new File(jarPath).toPath());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if (mappingPath != null) {
            System.out.println("loading mapping");
            try {
                mappingData = Files.readAllBytes(new File(mappingPath).toPath());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("parsing mapping");

        var mapping = new ObfuscationMapping();
        var mappingReader = new ProGuardMappingReader(mappingData);
        mappingReader.read(new ProGuardMappingMaker(mapping));
        // mappingReader.read(new ProGuardPrinter());

        // @NOTE(traks) for some reason since MC 1.18, the server JAR is
        // contains another JAR in its META-INF that contains all the actual
        // server code. Didn't check the client but I assume it's the same.
        // Libraries are also contained in separate JARs inside the server JAR.
        // Unpack all those JARs in the output.

        // first pass: load super types into the mapping, because otherwise we
        // may not deobfuscate all class members (methods + fields)
        System.out.println("constructing deobfuscation tree");

        iterateJARFiles(jar, (zis, entry) -> {
            if (entry.getName().endsWith(".class")) {
                var reader = new ClassReader(zis);
                reader.accept(new SuperProvider(mapping), 0);
            }
        });

        mapping.compressSupers();

        var jarInputs = new HashSet<String>();

        if (decompile) {
            System.out.println("collecting inputs");

            // in between pass: collect all input files
            iterateJARFiles(jar, (zis, entry) -> {
                jarInputs.add(entry.getName());
            });
        }

        System.out.println("converting files");

        var jarOutputs = new HashSet<String>();

        // second pass: actually do the remapping
        try (var fos = new BufferedOutputStream(new FileOutputStream(output))) {
            var zos = new ZipOutputStream(fos);

            iterateJARFiles(jar, (zis, entry) -> {
                System.out.println("Processing: " + entry.getName());

                if (!entry.getName().endsWith(".class")) {
                    // just copy non-classes to the output JAR

                    if (jarOutputs.contains(entry.getName())) {
                        // @NOTE(traks) Some .java files are already in the JAR
                        // so decompiling them will result in duplicate JAR
                        // entries. Avoid throwing an error!
                        System.out.println("duplicate jar entry " + entry.getName());
                        return;
                    }

                    jarOutputs.add(entry.getName());

                    zos.putNextEntry(new ZipEntry(entry));
                    zos.write(zis.readAllBytes());
                    zos.closeEntry();
                    return;
                }

                var reader = new ClassReader(zis);
                // reader.accept(new TraceClassVisitor(null, new Textifier(), new PrintWriter(System.out)), 0);

                var deobfuscator = new Deobfuscator(mapping);
                var writer = new ClassWriter(0);
                ClassVisitor remapper = writer;
                if (!keepLocals) {
                    remapper = new LocalVariableClassRemapper(remapper, deobfuscator);
                }
                remapper = new ClassRemapper(remapper, deobfuscator);
                reader.accept(remapper, 0);

                var remappedClass = writer.toByteArray();

                var obfuscatedType = Type.getObjectType(reader.getClassName());
                var remappedName = mapping.deobfuscateClass(obfuscatedType).getInternalName();

                if (!decompile) {
                    String obfName = obfuscatedType.getClassName() + ".class";
                    String newPath = entry.getName();

                    if (newPath.endsWith(obfName)) {
                        var basePath = newPath.substring(0, newPath.length() - obfName.length());
                        newPath = basePath + remappedName + ".class";
                    }

                    // System.out.println(entry.getName() + " -> " + newPath);

                    if (jarOutputs.contains(newPath)) {
                        System.out.println("deobfuscated entry " + newPath + " already present");
                        return;
                    }

                    jarOutputs.add(newPath);

                    zos.putNextEntry(new ZipEntry(newPath));
                    zos.write(remappedClass);
                    zos.closeEntry();
                } else {
                    String obfName = obfuscatedType.getClassName() + ".java";
                    String newPath = entry.getName();
                    // remove .class
                    newPath = newPath.substring(0, newPath.length() - 6) + ".java";

                    if (newPath.endsWith(obfName)) {
                        var basePath = newPath.substring(0, newPath.length() - obfName.length());
                        newPath = basePath + remappedName + ".java";
                    }

                    // System.out.println(entry.getName() + " -> " + newPath);

                    if (jarInputs.contains(newPath)) {
                        // @NOTE(traks) Some .java files are already in the JAR
                        // so decompiling them will result in duplicate JAR
                        // entries. Try using the provided source file.
                        return;
                    }

                    if (jarOutputs.contains(newPath)) {
                        System.out.println("decompiled entry " + newPath + " already present");
                        return;
                    }

                    jarOutputs.add(newPath);

                    var loader = new ByteArrayLoader(remappedClass, remappedName);
                    var printer = new StringPrinter();
                    var decompiler = new ClassFileToJavaSourceDecompiler();
                    try {
                        decompiler.decompile(loader, printer, remappedName);
                    } catch (Exception e) {
                        System.out.println("failed to decompile " + remappedName);
                        return;
                    }

                    zos.putNextEntry(new ZipEntry(newPath));
                    zos.write(printer.toString().getBytes(StandardCharsets.UTF_8));
                    zos.closeEntry();
                }
            });

            // Without this call an invalid JAR will be generated... not sure
            // what's going on there...
            zos.finish();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("done");
    }

    public static void main(String[] args) {
        var cmd = new Hopper();
        var cmdLine = new CommandLine(cmd);
        cmdLine.setColorScheme(CommandLine.Help.defaultColorScheme(
                CommandLine.Help.Ansi.OFF));
        System.exit(cmdLine.execute(args));
    }
}
