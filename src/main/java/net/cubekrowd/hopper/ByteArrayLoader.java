package net.cubekrowd.hopper;

import org.jd.core.v1.api.loader.Loader;

public final class ByteArrayLoader implements Loader {
    private final byte[] classData;
    private final String internalName;

    public ByteArrayLoader(byte[] classData, String internalName) {
        this.classData = classData;
        this.internalName = internalName;
    }

    @Override
    public boolean canLoad(String internalName) {
        return this.internalName.equals(internalName);
    }

    @Override
    public byte[] load(String internalName) {
        if (!canLoad(internalName)) {
            return null;
        }
        return classData;
    }
}
