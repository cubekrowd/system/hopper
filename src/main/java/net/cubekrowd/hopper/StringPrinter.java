package net.cubekrowd.hopper;

import org.jd.core.v1.api.printer.Printer;

public final class StringPrinter implements Printer {
    private static final String INDENT = "    ";

    private int indentation = 0;
    private StringBuilder sb = new StringBuilder();

    @Override
    public void start(int maxLineNumber, int majorVersion, int minorVersion) {}

    @Override
    public void end() {}

    // Traks: For some reason deobfuscating Minecraft 1.15.2 yields a bunch of
    // parameters with the unicode code point \u2603 as name, which is illegal
    // in Java source files. Here we try to avoid printing such code points
    // using chars, because I was unable to get it to work properly using code
    // point iteration (a bunch of files failed to decompile).
    private void appendEscaped(String text) {
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c > 127) {
                sb.append("u").append(Integer.toHexString(c));
            } else {
                sb.append(c);
            }
        }
    }

    @Override
    public void printText(String text) {
        appendEscaped(text);
    }

    @Override
    public void printNumericConstant(String constant) {
        sb.append(constant);
    }

    @Override
    public void printStringConstant(String constant, String ownerInternalName) {
        sb.append(constant);
    }

    @Override
    public void printKeyword(String keyword) {
        sb.append(keyword);
    }

    @Override
    public void printDeclaration(int type, String internalTypeName, String name,
            String descriptor) {
        sb.append(name);
    }

    @Override
    public void printReference(int type, String internalTypeName, String name,
            String descriptor, String ownerInternalName) {
        sb.append(name);
    }

    @Override
    public void indent() {
        indentation++;
    }

    @Override
    public void unindent() {
        indentation--;
    }

    @Override
    public void startLine(int lineNumber) {
        for (int i = 0; i < indentation; i++) {
            sb.append(INDENT);
        }
    }

    @Override
    public void endLine() {
        sb.append(System.lineSeparator());
    }

    @Override
    public void extraLine(int count) {
        for (int i = 0; i < count; i++) {
            sb.append(System.lineSeparator());
        }
    }

    @Override
    public void startMarker(int type) {}

    @Override
    public void endMarker(int type) {}

    @Override
    public String toString() {
        return sb.toString();
    }
}
