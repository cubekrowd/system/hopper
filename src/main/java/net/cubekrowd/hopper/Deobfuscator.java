package net.cubekrowd.hopper;

import net.cubekrowd.hopper.mapping.MemberId;
import net.cubekrowd.hopper.mapping.ObfuscationMapping;
import org.objectweb.asm.Type;

public final class Deobfuscator extends Remapper2 {
    private final ObfuscationMapping mapping;

    public Deobfuscator(ObfuscationMapping mapping) {
        this.mapping = mapping;
    }

    @Override
    public String mapAnnotationAttributeName(String descriptor, String name) {
        // @TODO(traks)
        return name;
    }

    @Override
    public String mapInnerClassName(String name, String ownerName, String innerName) {
        // @NOTE(traks) super handles this correctly for Java inner classes
        return super.mapInnerClassName(name, ownerName, innerName);
    }

    @Override
    public String mapMethodName(String owner, String name, String descriptor) {
        // @NOTE(traks) owner is internal name of containing class
        var classType = Type.getObjectType(owner);
        var methodType = Type.getType(descriptor);

        // @NOTE(traks) for record component accessor methods, the descriptor
        // will look like a field descriptor (just the type)!
        if (methodType.getSort() != Type.METHOD) {
            return mapFieldName(owner, name, descriptor);
        }

        var id = new MemberId(methodType, name);
        var res = mapping.deobfuscateMember(classType, id);
        return res;
    }

    @Override
    public String mapInvokeDynamicMethodName(String name, String descriptor) {
        // @TODO(traks)
        return name;
    }

    @Override
    public String mapRecordComponentName(String owner, String name, String descriptor) {
        return mapFieldName(owner, name, descriptor);
    }

    @Override
    public String mapFieldName(String owner, String name, String descriptor) {
        // @NOTE(traks) owner is internal name of containing class
        var classType = Type.getObjectType(owner);
        var fieldType = Type.getType(descriptor);
        var id = new MemberId(fieldType, name);
        var res = mapping.deobfuscateMember(classType, id);
        return res;
    }

    @Override
    public String mapPackageName(String name) {
        return super.mapPackageName(name);
    }

    @Override
    public String mapModuleName(String name) {
        return super.mapModuleName(name);
    }

    @Override
    public String map(String internalName) {
        var res = mapping.deobfuscateClass(Type.getObjectType(internalName)).getInternalName();
        return res;
    }

    // @NOTE(traks) Mojang compiles obfuscated local variable names as debugging
    // symbols into its class files, so we can easily remap them.
    @Override
    public String mapLocalVariableName(String name, String descriptor, int index) {
        if (name.equals("â˜ƒ")) {
            // @NOTE(traks) pre 1.18
            return "var" + index;
        } else if (name.startsWith("$$")) {
            // @NOTE(traks) post 1.18
            return "var" + index;
        } else {
            return name;
        }
    }
}
