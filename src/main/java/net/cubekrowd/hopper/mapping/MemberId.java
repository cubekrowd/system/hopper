package net.cubekrowd.hopper.mapping;

import java.util.Objects;
import java.util.StringJoiner;
import org.objectweb.asm.Type;

public final class MemberId {
    private final Type type;
    private final String name;

    public MemberId(Type type, String name) {
        this.type = type;
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MemberId)) {
            return false;
        }

        var o = (MemberId) obj;
        return o.type.equals(type) && o.name.equals(name);
    }

    @Override
    public String toString() {
        if (type.getSort() == Type.METHOD) {
            var res = type.getReturnType() + " " + name + "(";
            var params = new StringJoiner(", ");
            for (var param : type.getArgumentTypes()) {
                params.add(param.toString());
            }
            res += params + ")";
            return res;
        } else {
            return type + " " + name;
        }
    }
}
