package net.cubekrowd.hopper.mapping;

import org.objectweb.asm.Type;

public interface ProGuardMappingVisitor {
    void visitClass(Type obfuscatedType, Type deobfuscatedType);

    void endClass();

    void visitField(Type deobfuscatedType, String obfuscatedName,
            String deobfuscatedName);

    void visitMethod(Type deobfuscatedMethodType, String obfuscatedName,
            String deobfuscatedName);
}
