package net.cubekrowd.hopper.mapping;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.objectweb.asm.Type;

public final class ObfuscationMapping {
    public static final class ClassData {
        public final Type deobfuscatedType;
        public final Map<MemberId, String> memberMap = new HashMap<>(0);
        public final List<Type> supers = new ArrayList<>(0);

        public ClassData(Type deobfuscatedType) {
            this.deobfuscatedType = deobfuscatedType;
        }
    }

    public final Map<Type, ClassData> classMap = new HashMap<>();

    public boolean putClass(Type obfuscatedType, Type deobfuscatedType) {
        var currentData = classMap.putIfAbsent(obfuscatedType, new ClassData(deobfuscatedType));
        return currentData == null;
    }

    public boolean putMember(Type obfuscatedClassType, MemberId deobfuscatedId, String deobfuscatedName) {
        var classData = classMap.get(obfuscatedClassType);
        var currentName = classData.memberMap.putIfAbsent(deobfuscatedId, deobfuscatedName);
        return currentName == null;
    }

    public Type deobfuscateClass(Type obfuscatedType) {
        var classData = classMap.get(obfuscatedType);

        if (classData != null) {
            return classData.deobfuscatedType;
        } else {
            // System.out.println("unknown obfuscated type: " + obfuscatedType);
            return obfuscatedType;
        }
    }

    public String deobfuscateMember(Type obfuscatedClassType, MemberId obfuscatedId) {
        var classData = classMap.get(obfuscatedClassType);
        if (classData == null) {
            // System.out.println("unknown obfuscated member class: " + obfuscatedClassType);
            return obfuscatedId.getName();
        }

        Type deobfuscatedType;
        if (obfuscatedId.getType().getSort() == Type.METHOD) {
            var deobfuscatedReturnType = deobfuscateClass(obfuscatedId.getType().getReturnType());
            var obfuscatedParameterTypes = obfuscatedId.getType().getArgumentTypes();
            var deobfuscatedParameterTypes = new Type[obfuscatedParameterTypes.length];

            for (int i = 0; i < obfuscatedParameterTypes.length; i++) {
                deobfuscatedParameterTypes[i] = deobfuscateClass(obfuscatedParameterTypes[i]);
            }

            deobfuscatedType = Type.getMethodType(deobfuscatedReturnType, deobfuscatedParameterTypes);
        } else {
            // @NOTE(traks) field
            deobfuscatedType = deobfuscateClass(obfuscatedId.getType());
        }

        var deobfuscatedId = new MemberId(deobfuscatedType, obfuscatedId.getName());
        var res = classData.memberMap.get(deobfuscatedId);
        if (res == null) {
            // System.out.println("unknown obfuscated member: " + obfuscatedId.getName());
            res = obfuscatedId.getName();
        }
        return res;
    }

    public void addSuper(Type obfuscatedType, Type obfuscatedSuper) {
        var classData = classMap.get(obfuscatedType);

        if (classData != null) {
            classData.supers.add(obfuscatedSuper);
        }
    }

    public void compressSupers() {
        for (var classEntry : classMap.entrySet()) {
            var type = classEntry.getKey();
            var data = classEntry.getValue();
            var supers = new ArrayDeque<Type>();
            supers.addAll(data.supers);
            while (!supers.isEmpty()) {
                var sup = supers.pop();
                var supData = classMap.get(sup);
                if (supData != null) {
                    // @NOTE(traks) Super classes may contain private
                    // fields/methods with the same name and descriptor as a
                    // subtype due to deobfuscation. Be careful not to overwrite
                    // members of subtypes!
                    for (var superEntry : supData.memberMap.entrySet()) {
                        data.memberMap.putIfAbsent(superEntry.getKey(), superEntry.getValue());
                    }
                    supers.addAll(supData.supers);
                }
            }
        }
    }
}
