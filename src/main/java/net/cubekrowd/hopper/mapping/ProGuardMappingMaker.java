package net.cubekrowd.hopper.mapping;

import org.objectweb.asm.Type;

public final class ProGuardMappingMaker implements ProGuardMappingVisitor {
    private final ObfuscationMapping mapping;
    private Type currentClassType;

    public ProGuardMappingMaker(ObfuscationMapping mapping) {
        this.mapping = mapping;
    }

    @Override
    public void visitClass(Type obfuscatedType, Type deobfuscatedType) {
        mapping.putClass(obfuscatedType, deobfuscatedType);
        currentClassType = obfuscatedType;
    }

    @Override
    public void endClass() {
        currentClassType = null;
    }

    @Override
    public void visitField(Type deobfuscatedType, String obfuscatedName, String deobfuscatedName) {
        var id = new MemberId(deobfuscatedType, obfuscatedName);
        // @TODO handle false return?
        mapping.putMember(currentClassType, id, deobfuscatedName);
    }

    @Override
    public void visitMethod(Type deobfuscatedMethodType, String obfuscatedName, String deobfuscatedName) {
        var id = new MemberId(deobfuscatedMethodType, obfuscatedName);
        // @TODO handle false return?
        mapping.putMember(currentClassType, id, deobfuscatedName);
    }
}
