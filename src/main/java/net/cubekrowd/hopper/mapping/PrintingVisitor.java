package net.cubekrowd.hopper.mapping;

import org.objectweb.asm.Type;

public final class PrintingVisitor implements ProGuardMappingVisitor {
    @Override
    public void visitClass(Type obfuscatedType, Type deobfuscatedType) {
        System.out.println(deobfuscatedType.getDescriptor() + " -> " + obfuscatedType.getDescriptor());
    }

    @Override
    public void endClass() {
    }

    @Override
    public void visitField(Type deobfuscatedType, String obfuscatedName, String deobfuscatedName) {
        System.out.println("    " + deobfuscatedType.getDescriptor() + " " + deobfuscatedName + " -> " + obfuscatedName);
    }

    @Override
    public void visitMethod(Type deobfuscatedMethodType, String obfuscatedName, String deobfuscatedName) {
        System.out.println("    " + deobfuscatedMethodType.getDescriptor() + " " + deobfuscatedName + " -> " + obfuscatedName);
    }
}
