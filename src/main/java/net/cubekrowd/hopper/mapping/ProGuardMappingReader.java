package net.cubekrowd.hopper.mapping;

import java.util.ArrayList;
import org.objectweb.asm.Type;

public final class ProGuardMappingReader {
    public byte[] data;
    public int curIndex;
    public StringBuilder tempStringBuilder = new StringBuilder();

    private ProGuardMappingVisitor visitor;
    private boolean insideClass;
    private int lineIndex;

    public ProGuardMappingReader(byte[] data) {
        this.data = data;
    }

    public void ensureRemaining(int remaining) {
        if (data.length - curIndex < remaining) {
            throw new IllegalStateException("Not enough bytes remaining");
        }
    }

    public int readUTF8CodePoint() {
        int res;
        if ((data[curIndex] & 0x80) == 0) {
            res = data[curIndex];
            curIndex += 1;
        } else if ((data[curIndex] & 0xe0) == 0xc0) {
            ensureRemaining(2);
            res = ((data[curIndex] & 0x1f) << 6)
                    | (data[curIndex + 1] & 0x3f);
            curIndex += 2;
        } else if ((data[curIndex] & 0xf0) == 0xe0) {
            ensureRemaining(3);
            res = ((data[curIndex] & 0x0f) << 12)
                    | ((data[curIndex + 1] & 0x3f) << 6)
                    | (data[curIndex + 2] & 0x3f);
            curIndex += 3;
        } else if ((data[curIndex] & 0xf8) == 0xf0) {
            ensureRemaining(4);
            res = ((data[curIndex] & 0x07) << 18)
                    | ((data[curIndex + 1] & 0x3f) << 12)
                    | ((data[curIndex + 2] & 0x3f) << 6)
                    | (data[curIndex + 3] & 0x3f);
            curIndex += 4;
        } else {
            throw new IllegalStateException("Invalid UTF-8 data");
        }
        return res;
    }

    public void skipLine() {
        while (curIndex < data.length) {
            int codePoint = readUTF8CodePoint();
            if (codePoint == '\n') {
                break;
            }
        }
    }

    public void skipExact(String desired) {
        var codePointIter = desired.codePoints().iterator();
        while (codePointIter.hasNext()) {
            var cp = codePointIter.next();
            if (cp != readUTF8CodePoint()) {
                throw new IllegalStateException("Code point mismatch");
            }
        }
    }

    public String parseIdentifier() {
        tempStringBuilder.setLength(0);
        while (curIndex < data.length) {
            int start = curIndex;
            int cp = readUTF8CodePoint();
            if (cp == '(' || cp == ' ' || cp == '\n' || cp == '\r' || cp == ':') {
                curIndex = start;
                break;
            }
            tempStringBuilder.appendCodePoint(cp);
        }
        return tempStringBuilder.toString();
    }

    private boolean isDigit(int ch) {
        return '0' <= ch && ch <= '9';
    }

    private int parseLineNumber() {
        var num = parseIdentifier();
        skipExact(":");
        return Integer.parseInt(num);
    }

    private void parseClass() {
        var deobfuscatedType = parseType(false);
        skipExact(" -> ");
        var obfuscatedType = parseType(false);
        skipExact(":");
        visitor.visitClass(obfuscatedType, deobfuscatedType);
    }

    private Type parseType(boolean allowVoid) {
        tempStringBuilder.setLength(0);
        while (curIndex < data.length) {
            int start = curIndex;
            int cp = readUTF8CodePoint();
            if (cp == ' ' || cp == ')' || cp == ',' || cp == '\r' || cp == '\n' || cp == ':') {
                curIndex = start;
                break;
            }
            if (cp == '.') {
                // @NOTE(traks) replace dots by spaces to get the internal name
                // in case the type is a class (with package and all)
                cp = '/';
            }
            tempStringBuilder.appendCodePoint(cp);
        }

        // @NOTE(traks) Since types need not be classes, converting it into an
        // internal name doesn't always work. We must convert it to a type
        // descriptor instead.

        var typeName = tempStringBuilder.toString();

        if (allowVoid) {
            if (typeName.equals("void")) {
                return Type.VOID_TYPE;
            }
        }

        tempStringBuilder.setLength(0);
        // @NOTE(traks) first parse array stuff
        while (typeName.endsWith("[]")) {
            tempStringBuilder.append('[');
            typeName = typeName.substring(0, typeName.length() - 2);
        }

        // @NOTE(traks) now only the type (of the array) is left
        switch (typeName) {
        case "boolean": tempStringBuilder.append("Z"); break;
        case "char": tempStringBuilder.append("C"); break;
        case "byte": tempStringBuilder.append("B"); break;
        case "short": tempStringBuilder.append("S"); break;
        case "int": tempStringBuilder.append("I"); break;
        case "float": tempStringBuilder.append("F"); break;
        case "long": tempStringBuilder.append("J"); break;
        case "double": tempStringBuilder.append("D"); break;
        default:
            // @NOTE(traks) must be an object type
            tempStringBuilder.append("L").append(typeName).append(";");
        }

        return Type.getType(tempStringBuilder.toString());
    }

    private void parseField() {
        var deobfuscatedType = parseType(false);
        skipExact(" ");
        var deobfuscatedName = parseIdentifier();
        skipExact(" -> ");
        var obfuscatedName = parseIdentifier();
        visitor.visitField(deobfuscatedType, obfuscatedName, deobfuscatedName);
    }

    private void parseMethod() {
        // @NOTE(traks) A method starts with two line numbers or no line numbers
        // at all. If the first character is a digit, we try to parse line
        // numbers, otherwise not.
        int start = curIndex;
        if (isDigit(readUTF8CodePoint())) {
            curIndex = start;
            parseLineNumber();
            parseLineNumber();
        } else {
            curIndex = start;
        }

        var deobfuscatedReturnType = parseType(true);
        skipExact(" ");
        var deobfuscatedName = parseIdentifier();

        // @NOTE(traks) Now the parameter list. This is a comma-separated list
        // of deobfuscated types (if there are any types). Note that we already
        // consumed the opening bracket.

        var deobfuscatedParameterTypes = new ArrayList<Type>();
        skipExact("(");

        for (;;) {
            int paramStart = curIndex;
            int cp = readUTF8CodePoint();
            if (cp == ')') {
                break;
            } else if (cp == ',') {
                continue;
            }
            curIndex = paramStart;
            deobfuscatedParameterTypes.add(parseType(false));
        }

        skipExact(" -> ");
        var obfuscatedName = parseIdentifier();

        var deobfuscatedMethodType = Type.getMethodType(deobfuscatedReturnType, deobfuscatedParameterTypes.toArray(new Type[0]));
        visitor.visitMethod(deobfuscatedMethodType, obfuscatedName, deobfuscatedName);
    }

    private void parseMember() {
        // @NOTE(traks) A member is either a field or a method inside a class.
        // The only way to tell them apart is by checking whether the line
        // contains a round bracket (if so, it is a method, else a field).
        skipExact("    ");

        int start = curIndex;
        boolean isMethod = false;
        while (curIndex < data.length) {
            int cp = readUTF8CodePoint();
            if (cp == '\n') {
                break;
            }
            if (cp == '(') {
                isMethod = true;
                break;
            }
        }

        curIndex = start;
        if (isMethod) {
            parseMethod();
        } else {
            parseField();
        }
    }

    public void read(ProGuardMappingVisitor visitor) {
        this.visitor = visitor;
        while (curIndex < data.length) {
            int start = curIndex;
            int cp = readUTF8CodePoint();
            if (cp == '#') {
                // @NOTE(traks) comment, skip line
                skipLine();
            } else if (cp == ' ') {
                // @NOTE(traks)  Spaces indicate indentation, which is solely
                // used for fields and methods inside classes.
                curIndex = start;
                if (!insideClass) {
                    throw new IllegalStateException("method or field outside of class");
                }
                parseMember();
                skipLine();
            } else {
                // @NOTE(traks) otherwise this is a class definition
                curIndex = start;
                if (insideClass) {
                    visitor.endClass();
                }
                parseClass();
                insideClass = true;
                skipLine();
            }
        }
    }
}
