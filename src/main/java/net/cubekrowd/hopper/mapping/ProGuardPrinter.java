package net.cubekrowd.hopper.mapping;

import org.objectweb.asm.Type;

public class ProGuardPrinter implements ProGuardMappingVisitor {
    @Override
    public void visitClass(Type obfuscatedType, Type deobfuscatedType) {
        System.out.println(deobfuscatedType + " -> " + obfuscatedType + "{");
    }

    @Override
    public void endClass() {
        System.out.println("}");
    }

    @Override
    public void visitField(Type deobfuscatedType, String obfuscatedName, String deobfuscatedName) {
        System.out.println("    " + deobfuscatedType + " " + deobfuscatedName + " -> " + obfuscatedName);
    }

    @Override
    public void visitMethod(Type deobfuscatedMethodType, String obfuscatedName, String deobfuscatedName) {
        System.out.println("    " + deobfuscatedMethodType + " " + deobfuscatedName + " -> " + obfuscatedName);
    }
}
