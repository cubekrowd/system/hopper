# Hopper

A Minecraft client and server deobfuscator using Mojang's mappings. You can use this tool to generate a deobfuscated byte code JAR. This program also includes a built-in decompiler. See below for some of the available Java decompilers and how to use them.

This tool was designed to quickly obtain a readable version of Minecraft's code. This can help you to understand the game, and it facilitates developing Spigot plugins that rely on NMS.

## Usage

After downloading or cloning this Git repository, run the following command in the repository's directory to build the tool. Note that you will need Maven for this.

```bash
mvn clean package
```
To run the program, you can use the following:

```bash
java -jar target/hopper.jar
```
This will display a help page, which should be sufficient to fully grasp all functionality the program provides. To illustrate how simple this program is to use, consider the following command which downloads the server JAR and mappings file for Minecraft 1.15.2 and outputs a single file called 'deob.jar' containing the deobfuscated server byte code:

```bash
java -jar target/hopper.jar -sv 1.15.2 deob.jar
```

Instead of a version number, or 'release' or 'snapshot', you can also specify the path to a version JSON file. For example, download the experimental 1.18 ZIP linked [here](https://www.minecraft.net/en-us/article/new-world-generation-java-available-testing), unzip it and specify the path to the JSON file inside:

```bash
java -jar target/hopper.jar -sv path/to/1_18_experimental-snapshot-1.json deob.jar

```

## Decompilers

### Built-in

We use JD-Core to provide a built-in Java decompiler that can be used to produce a decompiled deobfuscated source code JAR in one go. Simply use the `-d` option. Here's an example that fetches the latest Minecraft server snapshot and decompiles it to 'decomp.jar':

```bash
java -jar target/hopper.jar -dsv snapshot decomp.jar
```
Simply unzip 'decomp.jar' to view the deobfuscated code.

### Fernflower

To obtain Fernflower, run the following commands in some appropriate location:

```bash
git clone --depth 1 https://github.com/JetBrains/intellij-community.git
cd intellij-community/plugins/java-decompiler/engine
# if you're not on Java 8, switch to it for gradlew
export JAVA_HOME=/path/to/java8/home
bash gradlew jar
```
This will output the Fernflower JAR as 'fernflower.jar' in 'build/libs'.
To decompile a deobfuscated byte code JAR produced by Hopper, you can for example use the following:

```bash
java -jar fernflower.jar -ind="    " -dgs=1 -rsy=1 /path/to/deobfuscated.jar /path/to/output/dir
```
Finally, unzip the output JAR.

### CFR

As of writing this, this decompiler is still maintained and supports many modern Java constructs.
You can download CFR from [here](https://github.com/leibnitz27/cfr/releases).
Then run the following command:

```bash
java -jar cfr.jar /path/to/deobfuscated.jar --outputdir /path/to/output/dir
```
